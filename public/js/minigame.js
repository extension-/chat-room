var socket = io.connect('https://minigame-dnh.herokuapp.com');

socket.on('sv_list_user', function(data) {
	console.log(data);
	$("#listUser").html("");
	data.forEach(function(user, index) {
		$("#listUser").append("<div class='user'>" +
			"<input type='text' readonly value='" + (index + 1) + "'>" +
			"<input type='text' readonly value='" + user.user_name + "'>" +
			"<input type='text' readonly value='" + user.email + "'>" +
			"<input type='text' readonly value='" + user.phone + "'>" +
			"</div>");
	})
});

/**-------------- JQUERY DOM -------------------*/
$(document).ready(function() {

	$("#registry").click(function() {
		let user_name = $("#user_name").val();
		let email     = $("#email").val();
		let phone     = $("#phone").val();
		socket.emit("user_registry", {
			user_name: user_name,
			email    : email,
			phone    : phone,
		});
	});

});
