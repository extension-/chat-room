var express = require('express');
var socket  = require('socket.io');
var app     = express();
app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");

var server = require("http").Server(app);
server.listen(process.env.port || 5000);
/**-------------------------------------- */

var io       = socket(server);
var arrUsers = [];
io.on('connection', function(socket) {
	socket.on('disconnect', function() {
		console.log('username out', socket.username);
	});

	socket.on('user_registry', function(data) {
		arrUsers.push(
			new User(data.user_name, data.email, data.phone),
		)
		io.sockets.emit("sv_list_user", arrUsers);
	});
});

/**-------------------------------------- */
function User(user_name, email, phone) {
	this.user_name = user_name;
	this.email     = email;
	this.phone     = phone;
}

app.get("/", function(req, res) {
	res.render("trangchu");
});
